defmodule AuroraASPWeb.AssignHelpers do
  alias AuroraASP.Assigns.Assign

  def generate_assign_badge(%Assign{} = assign) do
    generate_state_badge(AuroraASP.Assigns.get_assign_current_state(assign).state)
  end

  def generate_state_badge(state) do
    case state do
      :pending ->
        "<span class=\"assign-state-pending-badge\">Pendiente</span>"

      :active ->
        "<span class=\"assign-state-active-badge\">Activa</span>"

      :anulated ->
        "<span class=\"assign-state-anulated-badge\">Anulada</span>"

      :deserted ->
        "<span class=\"assign-state-deserted-badge\">Abandonada</span>"

      :completed ->
        "<span class=\"assign-state-completed-badge\">Completada</span>"
    end
  end

  def assign_humanized_state do
    Enum.map(Ecto.Enum.values(AuroraASP.Assigns.Assign.State, :state), fn s ->
      case s do
        :pending -> {"Pendiente", :pending}
        :active -> {"Activa", :active}
        :anulated -> {"Anulada", :anulated}
        :deserted -> {"Abandonada", :deserted}
        :completed -> {"Completada", :completed}
        other -> {other, other}
      end
    end)
  end

  def humanize_state(state) do
    case state do
      state when state in [:pending, "pending"] ->
        "Pendiente"

      state when state in [:active, "active"]  ->
        "Activa"

      state when state in [:anulated, "anulated"]  ->
        "Anulada"

      state when state in [:deserted, "deserted"] ->
        "Abandonada"

      state when state in [:completed, "completed"] ->
        "Completada"
    end
  end
end
