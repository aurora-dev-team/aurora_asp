defmodule AuroraASPWeb.EffortHelpers do

  def generate_lead_by_options() do
    members = Enum.map(AuroraASP.Persons.list_member_users(), fn u -> {u.name, u.id} end)
    [{"Sin responsable", nil}] ++ members
  end

  def generate_state_badge(state) do
    case state do
      :draft ->
        "<span class=\"effort-state-draft-badge\">Propuesta</span>"

      :active ->
        "<span class=\"effort-state-active-badge\">Abierto</span>"

      :archived ->
        "<span class=\"effort-state-archived-badge\">Archivado</span>"
    end
  end

  def effort_humanized_state do
    Enum.map(Ecto.Enum.values(AuroraASP.Efforts.Effort, :state), fn s ->
      case s do
        :draft -> {"Borrador", :draft}
        :active -> {"Abierto", :active}
        :archived -> {"Archivado", :archived}
        other -> {other, other}
      end
    end)
  end
end
