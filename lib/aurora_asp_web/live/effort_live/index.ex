defmodule AuroraASPWeb.EffortLive.Index do
  use AuroraASPWeb, :live_view

  alias AuroraASP.Efforts.Effort

  @impl true
  def mount(_params, session, socket) do
    # if connected?(socket), do: Aurora.EventBus.subscribe("effort")

    socket =
      socket
      |> assign_current_user(session)
      |> assign_client_timezone()
      |> assign(selected_effort: nil)
      |> assign(filter_status: ["active"])
      |> assign(filter_text: "")
      |> update_list()

    {:ok, socket, temporary_assigns: []}
  end

  @impl true
  def handle_params(%{"id" => effort_id}, _uri, socket) do
    case AuroraASP.Efforts.get_effort_assigns(effort_id) do
      %Effort{} = effort ->
        {:noreply, assign(socket, selected_effort: effort)}

      nil ->
        {:noreply, assign(socket, selected_effort: nil)}
    end
  end

  @impl true
  def handle_params(_, _uri, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_event("filter", %{"filter_text" => filter_text}, socket ) do

    socket =
      socket
      |> assign(filter_text: filter_text)
      |> update_list()

    {:noreply, socket}
  end

  @impl true
  def handle_event("update_state_filter", %{"state" => state} = _params, socket) do
    filter_status = socket.assigns.filter_status

    filter_status =
      if Enum.any?(filter_status, fn s -> state == s end) do
        filter_status -- [state]
      else
        filter_status ++ [state]
      end

    socket =
      socket
      |> assign(filter_status: filter_status)
      |> update_list()

    {:noreply, socket}
  end

  defp update_list(socket) do
    filter = [
      text: socket.assigns.filter_text,
      status: socket.assigns.filter_status
    ]

    effort_list = AuroraASP.Efforts.list_effort(filter)

    assign(socket, effort_list: effort_list)
  end
end
