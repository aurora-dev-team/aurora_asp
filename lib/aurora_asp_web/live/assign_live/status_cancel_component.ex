defmodule AuroraASPWeb.AssignLive.StatusCancelComponent do
  use AuroraASPWeb, :live_component

  alias AuroraASP.Assigns

  @impl true
  def update(assigns, socket) do
    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, Assigns.Assign.State.changeset(%AuroraASP.Assigns.Assign.State{}))}
  end

  @impl true
  def handle_event("validate", %{"state" => state_params}, socket) do
    changeset =
      %AuroraASP.Assigns.Assign.State{}
      |> AuroraASP.Assigns.Assign.State.changeset(state_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"state" => %{"message" => message}}, socket) do
    if socket.assigns.changeset.valid? do
      new_state = %AuroraASP.Assigns.Assign.State{
        message: message,
        state: :anulated,
        date: DateTime.utc_now()
      }

      case Assigns.update_assign_state(socket.assigns.assign, new_state) do
        {:ok, _assign} ->
          {:noreply,
           socket
           |> put_flash(:info, "Asignación anulada")
           |> push_redirect(to: socket.assigns.return_to)}

        {:error, %Ecto.Changeset{} = changeset} ->
          {:noreply, assign(socket, :changeset, changeset)}
      end
    else
      {:noreply,
       assign(socket, :changeset, Map.put(socket.assigns.changeset, :action, :validate))}
    end
  end
end
