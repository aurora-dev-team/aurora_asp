defmodule AuroraASPWeb.AssignLive.FormComponent do
  use AuroraASPWeb, :live_component

  alias AuroraASP.Assigns

  @impl true
  def update(%{assign: assign} = assigns, socket) do
    changeset = Assigns.change_assign(assign)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"assign" => assign_params}, socket) do
    changeset =
      socket.assigns.assign
      |> Assigns.change_assign(assign_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"assign" => assign_params}, socket) do
    save_assign(socket, socket.assigns.action, assign_params)
  end

  defp save_assign(socket, :edit, assign_params) do
    case Assigns.update_assign(socket.assigns.assign, assign_params) do
      {:ok, _assign} ->
        {:noreply,
         socket
         |> put_flash(:info, "Asignación actualizada")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_assign(socket, :new, assign_params) do
    assign_params = Map.put(assign_params, "status", [
      %{
        state: :pending,
        message: "Tu asignación será revisada por un miembro de la organización.",
        date: NaiveDateTime.utc_now()
      }
    ])
    case Assigns.create_assign(assign_params) do
      {:ok, assign} ->
        user = AuroraASP.Persons.get_user!(assign.user_id)
        DiscordNotifier.notify_new_assign(socket.assigns.effort, user)

        {:noreply,
         socket
         |> put_flash(:info, "Asignación postulada exitosamente")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
