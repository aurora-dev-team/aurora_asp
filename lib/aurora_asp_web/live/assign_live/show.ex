defmodule AuroraASPWeb.AssignLive.Show do
  use AuroraASPWeb, :live_view

  alias AuroraASP.Assigns
  alias AuroraASP.Efforts

  @impl true
  def mount(%{"effort_id" => effort_id}, session, socket) do
    effort = Efforts.get_effort!(effort_id)

    socket =
      socket
      |> assign(:effort, effort)
      |> assign_current_user(session)
      |> assign_client_timezone()

    {:ok, socket}
  end

  @impl true
  def handle_params(%{"assign_id" => assign_id}, _, socket) do
    {:noreply,
     socket
    #  |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:assign, Assigns.get_assign(assign_id))}
  end


  # defp page_title(:show), do: "Show Assign"
  # defp page_title(:status_update), do: "Actualizar estado asignación"
  # defp page_title(:edit), do: "Edit Assign"
end
