defmodule AuroraASPWeb.AssignLive.Index do
  use AuroraASPWeb, :live_view

  alias AuroraASP.Efforts
  alias AuroraASP.Assigns
  alias AuroraASP.Assigns.Assign

  @impl true
  def mount(%{"effort_id" => effort_id}, session, socket) do
    effort = Efforts.get_effort!(effort_id)

    socket = assign_current_user(socket, session)

    socket =
      socket
      |> assign_client_timezone()
      |> assign(:effort_assigns, list_effort_assigns(effort, socket.assigns.current_user))
      |> assign(:effort, effort)

    {:ok, socket}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"effort_id" => _effort_id, "assign_id" => assign_id}) do
    socket
    |> assign(:page_title, "Actualizar asignación")
    |> assign(:assign, Assigns.get_assign!(assign_id))
  end

  defp apply_action(socket, :new, %{"effort_id" => effort_id}) do
    socket
    |> assign(:page_title, "Nueva asignación")
    |> assign(:assign, %Assign{effort_id: effort_id, user_id: socket.assigns.current_user.id})
  end

  defp apply_action(socket, :index, _params) do
    socket
    # |> assign(:page_title, "Listing Effort assigns")
    |> assign(:assign, nil)
  end

  # @impl true
  # def handle_event("delete", %{"id" => id}, socket) do
  #   assign = Assigns.get_assign!(id)
  #   {:ok, _} = Assigns.delete_assign(assign)

  #   {:noreply, assign(socket, :effort_assigns, list_effort_assigns("asd", "asd"))}
  # end

  defp list_effort_assigns(effort, user) do
    case user.role do
      :helper ->
        Assigns.list_helper_effort_assigns(effort.id, user.id)

      :member ->
        Assigns.list_member_effort_assigns(effort.id)
    end
  end
end
