defmodule AuroraASPWeb.AssignLive.StatusUpdateComponent do
  use AuroraASPWeb, :live_component

  alias AuroraASP.Assigns

  @impl true
  def update(assigns, socket) do
    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, Assigns.Assign.State.changeset(%AuroraASP.Assigns.Assign.State{}))}
  end

  @impl true
  def handle_event("validate", %{"state" => state_params}, socket) do
    changeset =
      %AuroraASP.Assigns.Assign.State{}
      |> AuroraASP.Assigns.Assign.State.changeset(state_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"state" => state_params}, socket) do
    save_assign(socket, socket.assigns.action, state_params)
  end

  defp save_assign(socket, :status_update, %{"message" => state_message, "state" => state_state, "send_email_notify" => email_notify?} = _state_params) do
    if socket.assigns.changeset.valid? do
      new_state = %AuroraASP.Assigns.Assign.State{
        message: state_message,
        state: state_state,
        date: DateTime.utc_now()
      }

      case Assigns.update_assign_state(socket.assigns.assign, new_state) do
        {:ok, assign} ->

          if email_notify? == "true" do
            AuroraASP.Persons.UserNotifier.deliver_update_assign_update(assign.assign_user, assign.assign_effort, assign, new_state, Routes.effort_index_url(socket, :index, id: assign.assign_effort.id), Routes.assign_show_url(socket, :show, assign.assign_effort.id, assign.id))
          end

          {:noreply,
           socket
           |> put_flash(:info, "Asignación actualizada")
           |> push_redirect(to: socket.assigns.return_to)}

        {:error, %Ecto.Changeset{} = changeset} ->
          {:noreply, assign(socket, :changeset, changeset)}
      end
    else
      {:noreply, assign(socket, :changeset, Map.put(socket.assigns.changeset, :action, :validate))}
    end
  end

end
