defmodule AuroraASPWeb.Router do
  use AuroraASPWeb, :router

  import AuroraASPWeb.UserAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {AuroraASPWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AuroraASPWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", AuroraASPWeb do
  #   pipe_through :api
  # end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  ## Authentication routes

  scope "/", AuroraASPWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    get "/personas/registro", UserRegistrationController, :new
    post "/personas/registro", UserRegistrationController, :create
    get "/personas/ingreso", UserSessionController, :new
    post "/personas/ingreso", UserSessionController, :create
    get "/personas/recuperar", UserResetPasswordController, :new
    post "/personas/recuperar", UserResetPasswordController, :create
    get "/personas/recuperar/:token", UserResetPasswordController, :edit
    put "/personas/recuperar/:token", UserResetPasswordController, :update
  end

  scope "/", AuroraASPWeb do
    pipe_through [:browser, :require_authenticated_user]

    get "/personas/preferencias", UserSettingsController, :edit
    put "/personas/preferencias", UserSettingsController, :update
    get "/personas/preferencias/confirmar_email/:token", UserSettingsController, :confirm_email

    get "/esfuerzos/nuevo", EffortController, :new
    get "/esfuerzos/:id/editar", EffortController, :edit

    live "/esfuerzos/:effort_id/asignaciones", AssignLive.Index, :index
    live "/esfuerzos/:effort_id/asignaciones/nuevo", AssignLive.Index, :new
    live "/esfuerzos/:effort_id/asignaciones/:assign_id/editar", AssignLive.Index, :edit

    live "/esfuerzos/:effort_id/asignaciones/:assign_id", AssignLive.Show, :show
    live "/esfuerzos/:effort_id/asignaciones/:assign_id/ver/editar", AssignLive.Show, :edit
    live "/esfuerzos/:effort_id/asignaciones/:assign_id/estado/actualizar", AssignLive.Show, :status_update
    live "/esfuerzos/:effort_id/asignaciones/:assign_id/estado/anular", AssignLive.Show, :status_cancel
  end

  scope "/", AuroraASPWeb do
    pipe_through [:browser]

    delete "/personas/salir", UserSessionController, :delete
    get "/personas/confirmar", UserConfirmationController, :new
    post "/personas/confirmar", UserConfirmationController, :create
    get "/personas/confirmar/:token", UserConfirmationController, :edit
    post "/personas/confirmar/:token", UserConfirmationController, :update

    live "/esfuerzos", EffortLive.Index, :index
    resources "/esfuerzos", EffortController, except: [:new, :edit, :index]
    get "/colaboradores", PersonController, :index

    get "/faq", PageController, :faq
    get "/glosario", PageController, :glossary
    get "/guia_como_postular_esfuerzo", PageController, :guide_effort_apply

  end
end
