defmodule DiscordNotifier do
  alias AuroraASP.Efforts.Effort
  alias AuroraASP.Persons.User

  def notify_new_assign(
    %Effort{} = effort,
    %User{} = user
  ) do
    url = Application.get_env(:aurora_asp, :discord_webhook_url)

    user_name = if user.name == "", do: "Anónimo", else: user.name
    user_discord = if user.discord_user == "", do: "", else: "(#{user.discord_user})"
    effort_name = if effort.category == "", do: effort.name, else: "#{effort.name} / #{effort.category}"

    text = "Se ha postulado una nueva asignación en el esfuerzo #{effort_name} por #{user_name} #{user_discord}"

    HTTPoison.post(url, Jason.encode!(%{content: text}), [{"Content-Type", "application/json"}])
  end

end
