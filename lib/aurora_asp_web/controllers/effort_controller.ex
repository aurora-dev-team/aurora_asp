defmodule AuroraASPWeb.EffortController do
  use AuroraASPWeb, :controller

  alias AuroraASP.Efforts
  alias AuroraASP.Efforts.Effort

  def index(conn, _params) do
    effort = Efforts.list_effort()
    render(conn, "index.html", effort: effort)
  end

  def new(conn, _params) do
    changeset = Efforts.change_effort(%Effort{})
    render(conn, "new.html", changeset: changeset, phoenix_action: :new)
  end

  def create(conn, %{"effort" => effort_params}) do
    case Efforts.create_effort(effort_params, conn.assigns.current_user) do
      {:ok, effort} ->
        conn
        |> put_flash(:info, "Esfuerzo creado correctamente.")
        |> redirect(to: Routes.effort_index_path(conn, :index, id: effort.id))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, phoenix_action: :new)
    end
  end

  def show(conn, %{"id" => id}) do
    effort = Efforts.get_effort(id)
    render(conn, "show.html", effort: effort)
  end

  def edit(conn, %{"id" => id}) do
    effort = Efforts.get_effort!(id)
    changeset = Efforts.change_effort(effort)
    render(conn, "edit.html", effort: effort, changeset: changeset, phoenix_action: :edit)
  end

  def update(conn, %{"id" => id, "effort" => effort_params} = _params) do
    effort = Efforts.get_effort!(id)

    case Efforts.update_effort(effort, effort_params) do
      {:ok, effort} ->
        if effort_params["send_email_notify"] == "true" do
          Task.Supervisor.start_child(AuroraASP.AsyncEmailSupervisor, fn ->
            AuroraASP.Efforts.list_active_colaborators(effort.id)
            |> Enum.filter(fn u -> u.notify_updated_active_efforts end)
            |> Enum.each(fn u ->
              AuroraASP.Persons.UserNotifier.deliver_effort_update(u.email, effort, Routes.effort_index_url(conn, :index, id: effort.id))
            end)
          end)
        end
        conn
        |> put_flash(:info, "Esfuerzo actualizado correctamente.")
        |> redirect(to: Routes.effort_index_path(conn, :index, id: effort.id))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", effort: effort, changeset: changeset, phoenix_action: :edit)
    end
  end

  def delete(conn, %{"id" => id}) do
    effort = Efforts.get_effort!(id)
    {:ok, _effort} = Efforts.delete_effort(effort)

    conn
    |> put_flash(:info, "Esfuerzo eliminado correctamente.")
    |> redirect(to: Routes.effort_index_path(conn, :index))
  end
end
