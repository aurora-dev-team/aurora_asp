defmodule AuroraASPWeb.UserSessionController do
  use AuroraASPWeb, :controller

  alias AuroraASP.Persons
  alias AuroraASPWeb.UserAuth

  def new(conn, _params) do
    render(conn, "new.html", error_message: nil)
  end

  def create(conn, %{"user" => user_params}) do
    %{"email" => email, "password" => password} = user_params

    if user = Persons.get_user_by_email_and_password(email, password) do
      UserAuth.log_in_user(conn, user, user_params)
    else
      # In order to prevent user enumeration attacks, don't disclose whether the email is registered.
      render(conn, "new.html", error_message: "Credenciales inválidas")
    end
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "Sesión finalizada con éxito")
    |> UserAuth.log_out_user()
  end
end
