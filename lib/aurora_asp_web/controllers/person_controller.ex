defmodule AuroraASPWeb.PersonController do
  use AuroraASPWeb, :controller

  alias AuroraASP.Persons

  def index(conn, _params) do
    helpers_stadistics = Persons.list_helpers_stadistics()
    render(conn, "index.html", helpers_stadistics: helpers_stadistics)
  end

end
