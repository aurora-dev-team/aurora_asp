defmodule AuroraASPWeb.PageController do
  use AuroraASPWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def faq(conn, _params) do
    render(conn, "faq.html")
  end

  def guide_effort_apply(conn, _params) do
    render(conn, "guide_effort_apply.html")
  end

  def glossary(conn, _params) do
    render(conn, "glossary.html")
  end
end
