defmodule AuroraASPWeb.UserSettingsController do
  use AuroraASPWeb, :controller

  alias AuroraASP.Persons
  alias AuroraASPWeb.UserAuth

  plug :assign_changesets

  def edit(conn, _params) do
    render(conn, "edit.html")
  end

  def update(conn, %{"action" => "update_data"} = params) do
    %{"user" => user_params} = params
    user = conn.assigns.current_user

    case Persons.update_user_data(user, user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Datos actualizados con éxito.")
        |> render("edit.html", data_changeset: Persons.change_user_data(user))
      {:error, changeset} ->
        render(conn, "edit.html", data_changeset: Map.put(changeset, :action, :update))
    end
  end

  def update(conn, %{"action" => "update_email"} = params) do
    %{"current_password" => password, "user" => user_params} = params
    user = conn.assigns.current_user

    case Persons.apply_user_email(user, password, user_params) do
      {:ok, applied_user} ->
        Persons.deliver_update_email_instructions(
          applied_user,
          user.email,
          &Routes.user_settings_url(conn, :confirm_email, &1)
        )

        conn
        |> put_flash(
          :info,
          "Un enlace para confirmar el cambio de dirección ha sido enviado a tu nueva dirección."
        )
        |> redirect(to: Routes.user_settings_path(conn, :edit))

      {:error, changeset} ->
        render(conn, "edit.html", email_changeset: changeset)
    end
  end

  def update(conn, %{"action" => "update_password"} = params) do
    %{"current_password" => password, "user" => user_params} = params
    user = conn.assigns.current_user

    case Persons.update_user_password(user, password, user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Contraseña actualizada con éxito.")
        |> put_session(:user_return_to, Routes.user_settings_path(conn, :edit))
        |> UserAuth.log_in_user(user)

      {:error, changeset} ->
        render(conn, "edit.html", password_changeset: changeset)
    end
  end

  def confirm_email(conn, %{"token" => token}) do
    case Persons.update_user_email(conn.assigns.current_user, token) do
      :ok ->
        conn
        |> put_flash(:info, "Dirección actualizada con éxito.")
        |> redirect(to: Routes.user_settings_path(conn, :edit))

      :error ->
        conn
        |> put_flash(:error, "El enlace es inválido o ha expirado.")
        |> redirect(to: Routes.user_settings_path(conn, :edit))
    end
  end

  defp assign_changesets(conn, _opts) do
    user = conn.assigns.current_user

    conn
    |> assign(:data_changeset, Persons.change_user_data(user))
    |> assign(:email_changeset, Persons.change_user_email(user))
    |> assign(:password_changeset, Persons.change_user_password(user))

  end
end
