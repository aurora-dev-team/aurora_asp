defmodule AuroraASP.Assigns do
  @moduledoc """
  The Assigns context.
  """

  import Ecto.Query, warn: false
  alias AuroraASP.Repo

  alias AuroraASP.Assigns.Assign

  @doc """
  Returns the list of effort_assigns.

  ## Examples

      iex> list_effort_assigns()
      [%Assign{}, ...]

  """
  def list_effort_assigns do
    Repo.all(Assign)
  end

  @doc """
  Gets a single assign.

  Raises `Ecto.NoResultsError` if the Assign does not exist.

  ## Examples

      iex> get_assign!(123)
      %Assign{}

      iex> get_assign!(456)
      ** (Ecto.NoResultsError)

  """
  def get_assign!(id), do: Repo.get!(Assign, id)

  def get_assign(id) do
    Assign
    |> Repo.get(id)
    |> Repo.preload(:assign_user)
    |> Repo.preload(:assign_effort)
  end

  @doc """
  Creates a assign.

  ## Examples

      iex> create_assign(%{field: value})
      {:ok, %Assign{}}

      iex> create_assign(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_assign(attrs \\ %{}) do
    %Assign{}
    |> Assign.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a assign.

  ## Examples

      iex> update_assign(assign, %{field: new_value})
      {:ok, %Assign{}}

      iex> update_assign(assign, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_assign(%Assign{} = assign, attrs) do
    assign
    |> Assign.changeset(attrs)
    |> Repo.update()
  end

  def update_assign_state(%Assign{} = assign, state) do
    assign
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.put_embed(:status, assign.status ++ [state])
    |> Ecto.Changeset.cast_embed(:status)
    |> Repo.update()
  end

  @doc """
  Deletes a assign.

  ## Examples

      iex> delete_assign(assign)
      {:ok, %Assign{}}

      iex> delete_assign(assign)
      {:error, %Ecto.Changeset{}}

  """
  def delete_assign(%Assign{} = assign) do
    Repo.delete(assign)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking assign changes.

  ## Examples

      iex> change_assign(assign)
      %Ecto.Changeset{data: %Assign{}}

  """
  def change_assign(%Assign{} = assign, attrs \\ %{}) do
    Assign.changeset(assign, attrs)
  end

  def list_helper_effort_assigns(effort_id, user_id) do
    Repo.all(
      from a in Assign,
        where: a.effort_id == ^effort_id and a.user_id == ^user_id,
        order_by: [{:desc, a.inserted_at}]
    )

    # Repo.all(Effort)
  end

  def list_member_effort_assigns(effort_id) do
    Repo.all(
      from a in Assign,
        where: a.effort_id == ^effort_id,
        join: u in assoc(a, :assign_user),
        preload: [assign_user: u],
        order_by: [{:desc, a.inserted_at}]
    )
  end

  def get_assign_current_state(%Assign{} = assign) when is_list(assign.status) do
    assign.status
    |> Enum.reverse()
    |> Enum.at(0)
  end

  def count_completed_assigns() do
    Repo.one(
      from a in Assign,
        where:
          fragment("status -> (jsonb_array_length(status) - 1) ->> 'state'") in ["completed"],
        select: count("*")
    )
  end
end
