defmodule AuroraASP.Repo do
  use Ecto.Repo,
    otp_app: :aurora_asp,
    adapter: Ecto.Adapters.Postgres
end
