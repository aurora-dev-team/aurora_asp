defmodule AuroraASP.Efforts do
  @moduledoc """
  The Efforts context.
  """

  import Ecto.Query, warn: false
  alias AuroraASP.Repo

  alias AuroraASP.Persons.User
  alias AuroraASP.Efforts.Effort
  alias AuroraASP.Assigns.Assign

  @doc """
  Returns the list of effort.

  ## Examples

      iex> list_effort()
      [%Effort{}, ...]

  """
  def list_effort do
    Repo.all(Effort)
  end

  def list_effort(criteria) when is_list(criteria) do
    query = from(e in Effort, order_by: [{:desc, e.updated_at}, {:desc, e.inserted_at}])

    query =
      Enum.reduce(criteria, query, fn
        {:text, ""}, query ->
          query

        {:text, text}, query ->
          text = "%" <> text <> "%"
          from q in query, where: ilike(q.name, ^text)

        {:status, []}, query ->
          query

        {:status, status}, query ->
          from q in query, where: q.state in ^status
      end)

    Repo.all(query)
  end

  @doc """
  Gets a single effort.

  Raises `Ecto.NoResultsError` if the Effort does not exist.

  ## Examples

      iex> get_effort!(123)
      %Effort{}

      iex> get_effort!(456)
      ** (Ecto.NoResultsError)

  """
  def get_effort!(id), do: Repo.get!(Effort, id)

  def get_effort(id) do
    Effort
    |> Repo.get(id)
    |> Repo.preload(:created_by_user)
    |> Repo.preload(:lead_by_user)
  end

  def get_effort_assigns(id) do
    Effort
    |> Repo.get(id)
    |> Repo.preload(:created_by_user)
    |> Repo.preload(:lead_by_user)
    |> Repo.preload(:effort_assigns)
  end

  @doc """
  Creates a effort.

  ## Examples

      iex> create_effort(%{field: value})
      {:ok, %Effort{}}

      iex> create_effort(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_effort(attrs \\ %{}, created_by_user) do
    %Effort{}
    |> Effort.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:created_by_user, created_by_user)
    |> Repo.insert()
  end

  @doc """
  Updates a effort.

  ## Examples

      iex> update_effort(effort, %{field: new_value})
      {:ok, %Effort{}}

      iex> update_effort(effort, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_effort(%Effort{} = effort, attrs) do
    effort
    |> Effort.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a effort.

  ## Examples

      iex> delete_effort(effort)
      {:ok, %Effort{}}

      iex> delete_effort(effort)
      {:error, %Ecto.Changeset{}}

  """
  def delete_effort(%Effort{} = effort) do
    Repo.delete(effort)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking effort changes.

  ## Examples

      iex> change_effort(effort)
      %Ecto.Changeset{data: %Effort{}}

  """
  def change_effort(%Effort{} = effort, attrs \\ %{}) do
    Effort.changeset(effort, attrs)
  end

  def count_open_efforts() do
    Repo.one(from e in Effort, where: e.state == :active, select: count("*"))
  end

  def list_active_colaborators(effort_id) do
    Repo.all(
      from a in Assign,
        join: u in User,
        on: u.id == a.user_id,
        where:
          fragment("status -> (jsonb_array_length(status) - 1) ->> 'state'") in [
            "pending",
            "active"
          ],
        where: a.effort_id == ^effort_id,
        distinct: u.id,
        select: u
    )
  end
end
