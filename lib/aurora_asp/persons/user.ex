defmodule AuroraASP.Persons.User do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @timestamps_opts [type: :utc_datetime]
  schema "asp_users" do
    field :email, :string
    field :password, :string, virtual: true, redact: true
    field :hashed_password, :string, redact: true
    field :confirmed_at, :naive_datetime

    field :name, :string, default: ""
    field :discord_user, :string
    field :role, Ecto.Enum, values: [:member, :helper], default: :helper

    field :notify_new_efforts, :boolean, default: true
    field :notify_updated_active_efforts, :boolean, default: true

    field :additional_contact, :string
    field :social_contact, :string

    timestamps()
  end

  @doc """
  A user changeset for registration.

  It is important to validate the length of both email and password.
  Otherwise databases may truncate the email without warnings, which
  could lead to unpredictable or insecure behaviour. Long passwords may
  also be very expensive to hash for certain algorithms.

  ## Options

    * `:hash_password` - Hashes the password so it can be stored securely
      in the database and ensures the password field is cleared to prevent
      leaks in the logs. If password hashing is not needed and clearing the
      password field is not desired (like when using this changeset for
      validations on a LiveView form), this option can be set to `false`.
      Defaults to `true`.
  """
  def registration_changeset(user, attrs, opts \\ []) do
    user
    |> cast(attrs, [:email, :password, :name, :discord_user, :notify_new_efforts, :notify_updated_active_efforts, :additional_contact, :social_contact])
    |> validate_email()
    |> validate_password(opts)
    |> validate_name()
    |> validate_discord_user()
    |> validate_additional_contact()
  end

  defp validate_email(changeset) do
    changeset
    |> validate_required([:email], message: "Debes completar este campo")
    |> validate_format(:email, ~r/^[^\s]+@[^\s]+$/, message: "Debe contener ser una dirección de correo electrónica válida")
    |> validate_length(:email, max: 160)
    |> unsafe_validate_unique(:email, AuroraASP.Repo, message: "Esta dirección ya se encuentra en uso")
    |> unique_constraint(:email, message: "Esta dirección ya se encuentra en uso")
  end

  defp validate_password(changeset, opts) do
    changeset
    |> validate_required([:password], message: "Debes completar este campo")
    |> validate_length(:password, min: 6, message: "Debe contener al menos 6 caracteres")
    |> validate_length(:password, max: 20, message: "Debe contener máximo 20 caracteres")
    # |> validate_format(:password, ~r/[a-z]/, message: "at least one lower case character")
    # |> validate_format(:password, ~r/[A-Z]/, message: "at least one upper case character")
    # |> validate_format(:password, ~r/[!?@#$%^&*_0-9]/, message: "at least one digit or punctuation character")
    |> maybe_hash_password(opts)
  end

  defp validate_name(changeset) do
    changeset
    |> validate_length(:name, max: 40, message: "No puede contener más de 40 caracteres")
  end

  defp validate_discord_user(changeset) do
    changeset
    |> validate_required(:discord_user, message: "Debes completar este campo")
    |> validate_length(:discord_user, max: 32, message: "No puede contener más de 32 caracteres")
    |> validate_format(:discord_user, ~r/^(.)+([#]){1}([0-9]{4})$/, message: "Debes ingresar tu nombre de usuario de Discord. Este tiene el siguiente formato: usuario#4321")
  end

  defp validate_additional_contact(changeset) do
    changeset
    |> validate_length(:additional_contact, max: 40, message: "No puede contener más de 40 caracteres")
    |> validate_format(:additional_contact, ~r/^[\+]{1}[0-9]*$/, message: "Formato inválido. Debe comenzar por un \+ seguido sólo de números. Incluye tu número completo, no olvides el código del país.")
    |> validate_length(:social_contact, max: 40, message: "No puede contener más de 40 caracteres")
    |> validate_format(:social_contact, ~r/^[@]{1}[A-z0-9_.]+$/, message: "Formato inválido. Debe comenzar por un @ seguido sólo de números, letras, puntos  y/o guiones bajos.")
  end

  defp maybe_hash_password(changeset, opts) do
    hash_password? = Keyword.get(opts, :hash_password, true)
    password = get_change(changeset, :password)

    if hash_password? && password && changeset.valid? do
      changeset
      |> put_change(:hashed_password, Pbkdf2.hash_pwd_salt(password))
      |> delete_change(:password)
    else
      changeset
    end
  end

  @spec email_changeset(
          {map, map}
          | %{
              :__struct__ => atom | %{:__changeset__ => map, optional(any) => any},
              optional(atom) => any
            },
          :invalid | %{optional(:__struct__) => none, optional(atom | binary) => any}
        ) :: Ecto.Changeset.t()
  @doc """
  A user changeset for changing the email.

  It requires the email to change otherwise an error is added.
  """
  def email_changeset(user, attrs) do
    user
    |> cast(attrs, [:email])
    |> validate_email()
    |> case do
      %{changes: %{email: _}} = changeset -> changeset
      %{} = changeset -> add_error(changeset, :email, "No hubo cambio de dirección")
    end
  end

  @doc """
  A user changeset for changing the password.

  ## Options

    * `:hash_password` - Hashes the password so it can be stored securely
      in the database and ensures the password field is cleared to prevent
      leaks in the logs. If password hashing is not needed and clearing the
      password field is not desired (like when using this changeset for
      validations on a LiveView form), this option can be set to `false`.
      Defaults to `true`.
  """
  def password_changeset(user, attrs, opts \\ []) do
    user
    |> cast(attrs, [:password])
    |> validate_confirmation(:password, message: "Las contrasñas no coinciden")
    |> validate_password(opts)
  end

  @doc """
  Confirms the account by setting `confirmed_at`.
  """
  def confirm_changeset(user) do
    now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
    change(user, confirmed_at: now)
  end

  @doc """
  Verifies the password.

  If there is no user or the user doesn't have a password, we call
  `Pbkdf2.no_user_verify/0` to avoid timing attacks.
  """
  def valid_password?(%AuroraASP.Persons.User{hashed_password: hashed_password}, password)
      when is_binary(hashed_password) and byte_size(password) > 0 do
    Pbkdf2.verify_pass(password, hashed_password)
  end

  def valid_password?(_, _) do
    Pbkdf2.no_user_verify()
    false
  end

  @doc """
  Validates the current password otherwise adds an error to the changeset.
  """
  def validate_current_password(changeset, password) do
    if valid_password?(changeset.data, password) do
      changeset
    else
      add_error(changeset, :current_password, "Contraseña inválida")
    end
  end

  def data_changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :discord_user, :notify_new_efforts, :notify_updated_active_efforts, :additional_contact, :social_contact])
    |> validate_name()
    |> validate_discord_user()
    |> validate_additional_contact()
  end


end
