defmodule AuroraASP.Assigns.Assign do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @timestamps_opts [type: :utc_datetime]

  schema "effort_assigns" do
    belongs_to :assign_effort, AuroraASP.Efforts.Effort, foreign_key: :effort_id, type: :binary_id
    belongs_to :assign_user, AuroraASP.Persons.User, foreign_key: :user_id, type: :binary_id
    field :description, :string

    embeds_many :status, AuroraASP.Assigns.Assign.State

    timestamps()
  end

  @doc false
  def changeset(assign, attrs) do
    assign
    |> cast(attrs, [:effort_id, :user_id, :description])
    |> validate_required([:effort_id, :user_id, :description], message: "Este campo es requerido")
    |> validate_length(:description, min: 50, message: "Este campo debe contener al menos 50 caracteres")
    |> validate_length(:description, max: 500, message: "Este campo debe contener máximo 500 caracteres")
    |> cast_embed(:status)
  end
end

defmodule AuroraASP.Assigns.Assign.State do
  use Ecto.Schema
  alias Ecto.Changeset


  embedded_schema do
    field :state, Ecto.Enum, values: [:pending, :active, :completed, :anulated, :deserted], default: :pending
    field :message, :string
    field :date, :utc_datetime
  end

  def changeset(state, params \\ %{}) do
    state
    |> Changeset.cast(params, [
      :state,
      :message,
      :date
    ])
    |> Changeset.put_change(:date, DateTime.utc_now())
    |> Changeset.validate_required([:message, :date], message: "Este campo es requerido")
  end


end
