defmodule AuroraASP.Efforts.Effort do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  @timestamps_opts [type: :utc_datetime]
  schema "asp_efforts" do
    field :description, :string
    field :goal, :string
    field :name, :string
    field :state, Ecto.Enum, values: [:draft, :active, :archived], default: :draft
    field :category, :string, default: ""

    belongs_to :created_by_user, AuroraASP.Persons.User, foreign_key: :created_by, type: :binary_id
    belongs_to :lead_by_user, AuroraASP.Persons.User, foreign_key: :lead_by, type: :binary_id

    has_many :effort_assigns, AuroraASP.Assigns.Assign

    timestamps()
  end

  @doc false
  def changeset(effort, attrs) do
    effort
    |> cast(attrs, [:name, :goal, :description, :state, :lead_by, :category])
    |> validate_required([:name, :goal, :description, :state], message: "Este campo es requerido")
    |> validate_length(:name, max: 200, message: "Este campo debe contener máximo 200 caracteres")
    |> validate_length(:goal, max: 200, message: "Este campo debe contener máximo 200 caracteres")
  end
end
