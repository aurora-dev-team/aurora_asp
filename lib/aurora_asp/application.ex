defmodule AuroraASP.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      AuroraASP.Repo,
      # Start the Telemetry supervisor
      AuroraASPWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: AuroraASP.PubSub},
      # Async Email
      {Task.Supervisor, name: AuroraASP.AsyncEmailSupervisor},
      # Start the Endpoint (http/https)
      AuroraASPWeb.Endpoint
      # Start a worker by calling: AuroraASP.Worker.start_link(arg)
      # {AuroraASP.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: AuroraASP.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    AuroraASPWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
