defmodule AuroraASP.Repo.Migrations.CreateEffortAssigns do
  use Ecto.Migration

  def change do
    create table(:effort_assigns, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :effort_id, references(:asp_efforts, on_delete: :nothing, type: :binary_id), null: false
      add :user_id, references(:asp_users, on_delete: :nothing, type: :binary_id), null: false
      add :description, :text
      add :status, :map, null: false

      timestamps()
    end

    create index(:effort_assigns, [:effort_id])
    create index(:effort_assigns, [:user_id])
  end
end
