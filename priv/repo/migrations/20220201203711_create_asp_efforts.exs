defmodule AuroraASP.Repo.Migrations.CreateAspEfforts do
  use Ecto.Migration

  def change do
    create table(:asp_efforts, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :goal, :string
      add :description, :text
      add :state, :string
      add :category, :string

      add :created_by, references(:asp_users, on_delete: :nothing, type: :binary_id), null: false
      add :lead_by, references(:asp_users, on_delete: :nothing, type: :binary_id)

      timestamps()
    end
  end
end
