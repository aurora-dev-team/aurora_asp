defmodule AuroraASP.Repo.Migrations.AddUserPhoneField do
  use Ecto.Migration

  def change do
    alter table(:asp_users) do
      add :additional_contact, :string
      add :social_contact, :string
    end
  end
end
