defmodule AuroraASP.Repo.Migrations.CreateAspUsersAuthTables do
  use Ecto.Migration

  def change do
    execute "CREATE EXTENSION IF NOT EXISTS citext", ""

    create table(:asp_users, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :email, :citext, null: false
      add :hashed_password, :string, null: false
      add :confirmed_at, :naive_datetime

      add :name, :string, null: false
      add :discord_user, :string
      add :role, :string, null: false

      add :notify_new_efforts, :boolean, null: false, default: true
      add :notify_updated_active_efforts, :boolean, null: false, default: true

      timestamps()
    end

    create unique_index(:asp_users, [:email])

    create table(:asp_users_tokens) do
      add :user_id, references(:asp_users, on_delete: :delete_all, type: :binary_id), null: false
      add :token, :binary, null: false
      add :context, :string, null: false
      add :sent_to, :string
      timestamps(updated_at: false)
    end

    create index(:asp_users_tokens, [:user_id])
    create unique_index(:asp_users_tokens, [:context, :token])
  end
end
