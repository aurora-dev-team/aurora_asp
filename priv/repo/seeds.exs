# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     AuroraASP.Repo.insert!(%AuroraASP.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

# COLABORADORES

alias AuroraASP.Persons.User

for _i <- 1..10 do
  %User{}
  |> AuroraASP.Persons.User.registration_changeset(%{
    email: Faker.Internet.free_email(),
    name: Faker.Person.Es.name(),
    password: "123456",
    role: :helper,
    discord_user: Faker.Superhero.name() <> "#123"
  })
  |> AuroraASP.Repo.insert!()
end

for _i <- 1..5 do
  %User{}
  |> AuroraASP.Persons.User.registration_changeset(%{
    email: Faker.Internet.free_email(),
    name: Faker.Person.Es.name(),
    password: "123456",
    role: :member,
    discord_user: Faker.Superhero.name() <> "#123"
  })
  |> AuroraASP.Repo.insert!()
end

{:ok, helper} =
  %{
    email: "p.delgadohurtado@gmail.com",
    name: "Pavel Delgado",
    password: "123456",
    discord_user: "paveldegado#123"
  }
  |> AuroraASP.Persons.register_user()

{:ok, member} =
  %{
    email: "madesanzana@gmail.com",
    name: "Madeleine Sanzana",
    password: "123456",
    discord_user: "madesanzana#123",
    role: :member
  }
  |> AuroraASP.Persons.register_user()

alias AuroraASP.Efforts.Effort

for _i <- 1..100 do
  %Effort{
    id: Faker.UUID.v4(),
    name: Faker.Food.dish(),
    goal: Faker.Food.description(),
    description: Faker.Food.description(),
    state: Enum.random(Ecto.Enum.values(Effort, :state)),
    created_by: Enum.random([helper.id, member.id]),
  }
  |> AuroraASP.Repo.insert!()
end
