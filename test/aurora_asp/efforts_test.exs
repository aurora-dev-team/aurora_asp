defmodule AuroraASP.EffortsTest do
  use AuroraASP.DataCase

  alias AuroraASP.Efforts

  describe "effort" do
    alias AuroraASP.Efforts.Effort

    import AuroraASP.EffortsFixtures

    @invalid_attrs %{description: nil, goal: nil, name: nil, state: nil}

    test "list_effort/0 returns all effort" do
      effort = effort_fixture()
      assert Efforts.list_effort() == [effort]
    end

    test "get_effort!/1 returns the effort with given id" do
      effort = effort_fixture()
      assert Efforts.get_effort!(effort.id) == effort
    end

    test "create_effort/1 with valid data creates a effort" do
      valid_attrs = %{description: "some description", goal: "some goal", name: "some name", state: :draft}

      assert {:ok, %Effort{} = effort} = Efforts.create_effort(valid_attrs)
      assert effort.description == "some description"
      assert effort.goal == "some goal"
      assert effort.name == "some name"
      assert effort.state == :draft
    end

    test "create_effort/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Efforts.create_effort(@invalid_attrs)
    end

    test "update_effort/2 with valid data updates the effort" do
      effort = effort_fixture()
      update_attrs = %{description: "some updated description", goal: "some updated goal", name: "some updated name", state: :active}

      assert {:ok, %Effort{} = effort} = Efforts.update_effort(effort, update_attrs)
      assert effort.description == "some updated description"
      assert effort.goal == "some updated goal"
      assert effort.name == "some updated name"
      assert effort.state == :active
    end

    test "update_effort/2 with invalid data returns error changeset" do
      effort = effort_fixture()
      assert {:error, %Ecto.Changeset{}} = Efforts.update_effort(effort, @invalid_attrs)
      assert effort == Efforts.get_effort!(effort.id)
    end

    test "delete_effort/1 deletes the effort" do
      effort = effort_fixture()
      assert {:ok, %Effort{}} = Efforts.delete_effort(effort)
      assert_raise Ecto.NoResultsError, fn -> Efforts.get_effort!(effort.id) end
    end

    test "change_effort/1 returns a effort changeset" do
      effort = effort_fixture()
      assert %Ecto.Changeset{} = Efforts.change_effort(effort)
    end
  end
end
