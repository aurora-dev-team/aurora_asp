defmodule AuroraASP.AssignsTest do
  use AuroraASP.DataCase

  alias AuroraASP.Assigns

  describe "effort_assigns" do
    alias AuroraASP.Assigns.Assign

    import AuroraASP.AssignsFixtures

    @invalid_attrs %{description: nil}

    test "list_effort_assigns/0 returns all effort_assigns" do
      assign = assign_fixture()
      assert Assigns.list_effort_assigns() == [assign]
    end

    test "get_assign!/1 returns the assign with given id" do
      assign = assign_fixture()
      assert Assigns.get_assign!(assign.id) == assign
    end

    test "create_assign/1 with valid data creates a assign" do
      valid_attrs = %{description: "some description"}

      assert {:ok, %Assign{} = assign} = Assigns.create_assign(valid_attrs)
      assert assign.description == "some description"
    end

    test "create_assign/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Assigns.create_assign(@invalid_attrs)
    end

    test "update_assign/2 with valid data updates the assign" do
      assign = assign_fixture()
      update_attrs = %{description: "some updated description"}

      assert {:ok, %Assign{} = assign} = Assigns.update_assign(assign, update_attrs)
      assert assign.description == "some updated description"
    end

    test "update_assign/2 with invalid data returns error changeset" do
      assign = assign_fixture()
      assert {:error, %Ecto.Changeset{}} = Assigns.update_assign(assign, @invalid_attrs)
      assert assign == Assigns.get_assign!(assign.id)
    end

    test "delete_assign/1 deletes the assign" do
      assign = assign_fixture()
      assert {:ok, %Assign{}} = Assigns.delete_assign(assign)
      assert_raise Ecto.NoResultsError, fn -> Assigns.get_assign!(assign.id) end
    end

    test "change_assign/1 returns a assign changeset" do
      assign = assign_fixture()
      assert %Ecto.Changeset{} = Assigns.change_assign(assign)
    end
  end
end
