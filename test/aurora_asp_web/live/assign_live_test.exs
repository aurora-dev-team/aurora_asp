defmodule AuroraASPWeb.AssignLiveTest do
  use AuroraASPWeb.ConnCase

  import Phoenix.LiveViewTest
  import AuroraASP.AssignsFixtures

  @create_attrs %{description: "some description"}
  @update_attrs %{description: "some updated description"}
  @invalid_attrs %{description: nil}

  defp create_assign(_) do
    assign = assign_fixture()
    %{assign: assign}
  end

  describe "Index" do
    setup [:create_assign]

    test "lists all effort_assigns", %{conn: conn, assign: assign} do
      {:ok, _index_live, html} = live(conn, Routes.assign_index_path(conn, :index))

      assert html =~ "Listing Effort assigns"
      assert html =~ assign.description
    end

    test "saves new assign", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.assign_index_path(conn, :index))

      assert index_live |> element("a", "New Assign") |> render_click() =~
               "New Assign"

      assert_patch(index_live, Routes.assign_index_path(conn, :new))

      assert index_live
             |> form("#assign-form", assign: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#assign-form", assign: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.assign_index_path(conn, :index))

      assert html =~ "Assign created successfully"
      assert html =~ "some description"
    end

    test "updates assign in listing", %{conn: conn, assign: assign} do
      {:ok, index_live, _html} = live(conn, Routes.assign_index_path(conn, :index))

      assert index_live |> element("#assign-#{assign.id} a", "Edit") |> render_click() =~
               "Edit Assign"

      assert_patch(index_live, Routes.assign_index_path(conn, :edit, assign))

      assert index_live
             |> form("#assign-form", assign: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#assign-form", assign: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.assign_index_path(conn, :index))

      assert html =~ "Assign updated successfully"
      assert html =~ "some updated description"
    end

    test "deletes assign in listing", %{conn: conn, assign: assign} do
      {:ok, index_live, _html} = live(conn, Routes.assign_index_path(conn, :index))

      assert index_live |> element("#assign-#{assign.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#assign-#{assign.id}")
    end
  end

  describe "Show" do
    setup [:create_assign]

    test "displays assign", %{conn: conn, assign: assign} do
      {:ok, _show_live, html} = live(conn, Routes.assign_show_path(conn, :show, assign))

      assert html =~ "Show Assign"
      assert html =~ assign.description
    end

    test "updates assign within modal", %{conn: conn, assign: assign} do
      {:ok, show_live, _html} = live(conn, Routes.assign_show_path(conn, :show, assign))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Assign"

      assert_patch(show_live, Routes.assign_show_path(conn, :edit, assign))

      assert show_live
             |> form("#assign-form", assign: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#assign-form", assign: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.assign_show_path(conn, :show, assign))

      assert html =~ "Assign updated successfully"
      assert html =~ "some updated description"
    end
  end
end
