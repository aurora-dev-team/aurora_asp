defmodule AuroraASPWeb.EffortControllerTest do
  use AuroraASPWeb.ConnCase

  import AuroraASP.EffortsFixtures

  @create_attrs %{description: "some description", goal: "some goal", name: "some name", state: :draft}
  @update_attrs %{description: "some updated description", goal: "some updated goal", name: "some updated name", state: :active}
  @invalid_attrs %{description: nil, goal: nil, name: nil, state: nil}

  describe "index" do
    test "lists all effort", %{conn: conn} do
      conn = get(conn, Routes.effort_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Effort"
    end
  end

  describe "new effort" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.effort_path(conn, :new))
      assert html_response(conn, 200) =~ "New Effort"
    end
  end

  describe "create effort" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.effort_path(conn, :create), effort: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.effort_path(conn, :show, id)

      conn = get(conn, Routes.effort_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Effort"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.effort_path(conn, :create), effort: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Effort"
    end
  end

  describe "edit effort" do
    setup [:create_effort]

    test "renders form for editing chosen effort", %{conn: conn, effort: effort} do
      conn = get(conn, Routes.effort_path(conn, :edit, effort))
      assert html_response(conn, 200) =~ "Edit Effort"
    end
  end

  describe "update effort" do
    setup [:create_effort]

    test "redirects when data is valid", %{conn: conn, effort: effort} do
      conn = put(conn, Routes.effort_path(conn, :update, effort), effort: @update_attrs)
      assert redirected_to(conn) == Routes.effort_path(conn, :show, effort)

      conn = get(conn, Routes.effort_path(conn, :show, effort))
      assert html_response(conn, 200) =~ "some updated description"
    end

    test "renders errors when data is invalid", %{conn: conn, effort: effort} do
      conn = put(conn, Routes.effort_path(conn, :update, effort), effort: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Effort"
    end
  end

  describe "delete effort" do
    setup [:create_effort]

    test "deletes chosen effort", %{conn: conn, effort: effort} do
      conn = delete(conn, Routes.effort_path(conn, :delete, effort))
      assert redirected_to(conn) == Routes.effort_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.effort_path(conn, :show, effort))
      end
    end
  end

  defp create_effort(_) do
    effort = effort_fixture()
    %{effort: effort}
  end
end
