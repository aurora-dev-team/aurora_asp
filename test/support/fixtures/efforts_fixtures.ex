defmodule AuroraASP.EffortsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `AuroraASP.Efforts` context.
  """

  @doc """
  Generate a effort.
  """
  def effort_fixture(attrs \\ %{}) do
    {:ok, effort} =
      attrs
      |> Enum.into(%{
        description: "some description",
        goal: "some goal",
        name: "some name",
        state: :draft
      })
      |> AuroraASP.Efforts.create_effort()

    effort
  end
end
