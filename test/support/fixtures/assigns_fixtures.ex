defmodule AuroraASP.AssignsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `AuroraASP.Assigns` context.
  """

  @doc """
  Generate a assign.
  """
  def assign_fixture(attrs \\ %{}) do
    {:ok, assign} =
      attrs
      |> Enum.into(%{
        description: "some description"
      })
      |> AuroraASP.Assigns.create_assign()

    assign
  end
end
